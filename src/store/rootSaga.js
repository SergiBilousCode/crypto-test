import { all } from "redux-saga/effects";
import { cryptoCourseSaga } from "./cryptoCourse";

export default function*() {
	yield all([cryptoCourseSaga()]);
}
