import { put, call, delay, fork } from 'redux-saga/effects';
import axios from 'axios';
import convert from 'xml-js';
import { nationalCourseResponse, cryptoResponse } from './actios';

export function* cryptoCourseSaga() {
  while (true) {
    yield call(fetchAll);
  }
}

function* fetchAll() {
  const task1 = yield fork(nationalCourseRequest);
  const task2 = yield fork(cryptoRequest, 'XRP');
  const task3 = yield fork(cryptoRequest, 'ETH');
  yield delay(300000);
}

function* cryptoRequest(param) {
  try {
    const header = {
      headers: {
        'Access-Control-Allow-Origin': '*',
        'X-CoinAPI-Key': '54C1AC15-4A2C-4090-BED8-983980BC4C2A'
      }
    };
    const response = yield call([axios, axios.get], `https://rest.coinapi.io/v1/exchangerate/${param}/USD`, header);
    yield put(cryptoResponse(response.data));
  } catch (e) {
    console.log(e);
  }
}

function* nationalCourseRequest() {
  try {
    const response = yield call([axios, axios.get], `https://api.privatbank.ua/p24api/pubinfo?exchange&coursid=11`);
    const result = convert.xml2js(response.data, {
      compact: true,
      spaces: 2
    });
    yield put(nationalCourseResponse(result.exchangerates.row));
  } catch (e) {
    console.log(e);
  }
}
