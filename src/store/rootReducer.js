import actionType from './actionType';

const initialState = {
  RUR: 0,
  USD: 0,
  UAH: 0,
  BTC: 0,
  XRP: 0,
  ETH: 0
};

export default function rootReducer(state = initialState, action) {
  switch (action.type) {
    case actionType.NATIONAL_COURSE_RESPONSE:
      const array = action.payload;
      let course = {};
      array.forEach(value => {
        course[value.exchangerate._attributes.ccy] = value.exchangerate._attributes.buy;
      });
      return {
        ...state,
        ...course
      };
    case actionType.CRYPTO_RESPONSE:
      const obj = action.payload;
      let courseCrypto = {};
      courseCrypto[obj.asset_id_base] = obj.rate;
      return {
        ...state,
        ...courseCrypto
      };
    default:
      return state;
  }
}
