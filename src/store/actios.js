import actionType from "./actionType";

const { NATIONAL_COURSE_RESPONSE, CRYPTO_RESPONSE } = actionType;

export const nationalCourseResponse = array => ({
	type: NATIONAL_COURSE_RESPONSE,
	payload: array
});

export const cryptoResponse = obj => ({
	type: CRYPTO_RESPONSE,
	payload: obj
});
