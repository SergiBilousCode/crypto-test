import React, { Component, memo } from 'react';
import PropTypes from 'prop-types';
import styles from './CryptoCard.scss';

class CryptoCard extends Component {
  render() {
    const { currency, RUR, USD, selected, logo } = this.props;

    return (
      <div className={styles.wrap_card} onClick={() => selected(currency)}>
        <div className={styles.wrap_img}>
          <img src={logo} />
          <div className={styles.img_info}>{currency}</div>
        </div>
        <div className={styles.wrap_content}>
          <div className={styles.line}>
            <span>USD</span>
            <span>{Math.round(this.props[currency] * 10000) / 10000}</span>
          </div>
          <div className={styles.line}>
            <span>UAH</span>
            <span>{Math.round(this.props[currency] * USD * 10000) / 10000}</span>
          </div>
          <div className={styles.line}>
            <span>RUB</span>
            <span>{Math.round(((this.props[currency] * USD) / RUR) * 10000) / 10000}</span>
          </div>
        </div>
      </div>
    );
  }
}
export default memo(CryptoCard);

CryptoCard.protoType = {
  currency: PropTypes.string,
  RUR: PropTypes.number,
  USD: PropTypes.number,
  selected: PropTypes.func,
  logo: PropTypes.string
};

CryptoCard.defaultProps = {
  currency: '',
  RUR: 0,
  USD: 0,
  selected: () => {},
  logo: ''
};
