import React, { Component } from "react";
import styles from "./SectionInput.scss";
import cx from "classnames";

export class SectionInput extends Component {
	constructor(props) {
		super(props);
		this.state = {
			national: "UAH",
			value: 0
		};
	}

	changeNational = param => {
		this.setState({ national: param });
	};

	calculationOfAmount = () => {
		const { value, national } = this.state;
		const { selected, RUR, USD } = this.props;

		switch (national) {
			case "USD":
				return `${value} ${selected} will be ${Math.round(
					this.props[selected] * value * 10000
				) / 10000} in ${national}`;
			case "UAH":
				return `${value} ${selected} will be ${Math.round(
					this.props[selected] * USD * value * 10000
				) / 10000} in ${national}`;
			case "RUB":
				return `${value} ${selected} will be ${Math.round(
					((this.props[selected] * USD) / RUR) * value * 10000
				) / 10000} in ${national}`;
			default:
				return `${value} ${selected} will be ${0} in ${national}`;
		}
	};

	onChange = event => {
		this.setState({
			value: event.target.value === "" ? 0 : event.target.value
		});
	};

	render() {
		const { selected, BTC, XRP, ETH, UAH, RUR, USD } = this.props;
		const { national, value } = this.state;
		return (
			<div className={styles.section_input}>
				<h2>{`Selected coin: ${selected}`}</h2>
				<div className={styles.input_group}>
					<label htmlFor="crypto-input">Volume:</label>
					<input id="crypto-input" type="text" onChange={this.onChange} />
				</div>
				<div className={styles.btn_group}>
					<button
						className={cx([national === "UAH" && styles.active_btn])}
						onClick={() => this.changeNational("UAH")}
					>
						UAH
					</button>
					<button
						className={cx([national === "USD" && styles.active_btn])}
						onClick={() => this.changeNational("USD")}
					>
						USD
					</button>
					<button
						className={cx([national === "RUB" && styles.active_btn])}
						onClick={() => this.changeNational("RUB")}
					>
						RUB
					</button>
				</div>
				<div className={styles.main_info}>{this.calculationOfAmount()}</div>
			</div>
		);
	}
}
