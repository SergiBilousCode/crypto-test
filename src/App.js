import React, { useState } from 'react';
import { CryptoCard, SectionInput } from './components';
import styles from './App.scss';
import logo1 from '../public/logo1.jpeg';
import logo2 from '../public/logo2.jpeg';
import logo3 from '../public/logo3.jpeg';

function App() {
  const [selected, setSelected] = useState('BTC');

  const changeSelectedCourse = param => {
    setSelected(param);
  };

  return (
    <div className={styles.main_page}>
      <div className={styles.content}>
        <div className={styles.section_card}>
          <CryptoCard currency="BTC" selected={changeSelectedCourse} logo={logo1} />
          <CryptoCard currency="ETH" selected={changeSelectedCourse} logo={logo2} />
          <CryptoCard currency="XRP" selected={changeSelectedCourse} logo={logo3} />
        </div>
        <SectionInput selected={selected} />
      </div>
    </div>
  );
}

export default App;
